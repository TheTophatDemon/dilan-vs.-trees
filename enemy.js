const ANIM_SKELEWALK = [0, 1, 0, 2];
const ANIM_SKELEDIE = [3, 4, 5];

const SKELE_CHASE = 32;
function Skeleton(x, y, a)
{
    Enemy.call(this, x, y, a, 8.0);
    this.frameSize = 32;
    this.worldScale = this.frameSize * 2.5;
    this.frameX = 0;
    this.frameY = 304;
    this.flammable = true;
    this.fireTimer = 0;
    this.fireSpeed = 1.5;
    this.health = 100;
    this.walkSpeed = 64.0;
    this.shootTimer = 0;
    this.changeState(ENEMY_WANDER);
    this.aggro = false;
}
Skeleton.prototype = 
{
    update: function(deltaTime)
    {
        Enemy.prototype.update.call(this, deltaTime);
        if (this.onFire)
        {
            this.hurt(deltaTime * 10.0);
            this.fireTimer -= deltaTime;
            if (this.fireTimer <= 0)
            {
                this.fireTimer = 0;
                this.onFire = false;
            }
        }
        var distToPly = distance2DSqr(this.x, this.y , gGameState.player.x, gGameState.player.y);
        var angToPly = Math.atan2(gGameState.player.y - this.y, gGameState.player.x - this.x);
        //Throw bones
        if (this.state == ENEMY_WANDER)
        {
            this.shootTimer += deltaTime;
            if (this.shootTimer > 1.0 / (0.25 + gGameSetupState.options[OPT_DIFF].value * 0.75))
            {
                this.shootTimer = 0;
                if (distToPly < 9216.0) //96 * 96
                {
                    PlaySound("bone.wav", 0.5, true);
                    gGameState.addEnt(new Bone(this.x, this.y, 0.0, angToPly, this));
                }
            }
        }
        switch(this.state)
        {
            case ENEMY_WANDER:
                if (distToPly > 9216.0)
                {
                    if (this.aggro) this.a += (angToPly - this.a) * deltaTime * 10.0;  //Will chase you if they know you're there
                }
                else
                {
                    this.aggro = true; //They know you're there when your 96 units close to them
                }
                if (distToPly >= 262144) this.aggro = false; //They'll forget about you if you're too far away

                var diffScale = 0.25 + 0.75 * gGameSetupState.options[OPT_DIFF].value;
                var dx = Math.cos(this.a) * this.walkSpeed * deltaTime * diffScale;
                var dy = Math.sin(this.a) * this.walkSpeed * deltaTime * diffScale;
                var hit = this.tryMove(dx, dy, ["enemyType"]);
                if (hit)
                {
                    this.a += deltaTime;
                }
                break;
            case ENEMY_DEAD:
                if (this.stateTimer > 25.0)
                {
                    this.health = 100;
                    this.changeState(ENEMY_WANDER);
                }
                break;
        }
    },
    enterState: function(newState)
    {
        switch (newState)
        {
            case ENEMY_WANDER:
                this.setAnim(ANIM_SKELEWALK, 0.05, true);
                break;
            case ENEMY_DEAD:
                PlaySound("skeledeath.wav", 0.4, true);
                this.setAnim(ANIM_SKELEDIE, 0.2, false);
                if (Math.random() < 0.1)
                {
                    gGameState.addEnt(new Twinkie(this.x, this.y));
                }
                else
                {
                    gGameState.addEnt(new Crystal(this.x, this.y, 0));
                }
                break;
        }
    }
};
Object.setPrototypeOf(Skeleton.prototype, Enemy.prototype);