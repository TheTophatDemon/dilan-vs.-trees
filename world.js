function distance2D(x1, y1, x2, y2)
{
    return Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
}
function distance2DSqr(x1, y1, x2, y2)
{
    return (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1);
}
function distance3DSqr(x1, y1, z1, x2, y2, z2)
{
    return (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1);
}

var gTreeHealth = 15.0;
var gGrowthSpeed = 5.0; //Number of seconds it takes one tree to ascend one growth stage
function Tree(x, y, stage)
{
    Sprite.call(this, x, y, 0.0, 16.0);
    this.worldScale = this.frameSize * 2.5;
    this.growthTimer = 0.0;
    this.growthStage = stage;
    this.updateFrame();
    this.solid = false;
    if (this.growthStage > 0) this.solid = true;
    this.health = gTreeHealth;
    this.flammable = true;
    this.fireTimer = 0;
    this.fireSpeed = 3.0;
    this.fall = 0;
    this.newZ = this.z;
}
Tree.prototype = 
{
    canHurt: true,
    treeType: true,
    update: function(deltaTime)
    {
        if (this.newZ != this.z)
        {
            var diff = (this.newZ - this.z);
            if (Math.abs(diff) < 0.1)
            {
                this.z = this.newZ;
            }
            else
            {
                this.z += diff * 0.5;
            }
        }
        if (this.newZ != 0.0)
        {
            this.newZ -= deltaTime * this.newZ * 4.0;
        }
        if (this.fireTimer > 0)
        {
            this.fireTimer -= deltaTime;
            if (this.fireTimer <= 0)
            {
                this.onFire = false;
                this.fireTimer = 0;
            }
            this.hurt(deltaTime * 20.0);
        }
        else
        {
            this.growthTimer += deltaTime;
            if (this.growthTimer > gGrowthSpeed)
            {
                this.growthTimer = 0;
                if (this.growthStage < 4)
                {
                    this.growthStage++;
                    if (this.growthStage > 1 && !this.solid) 
                    {
                        this.solid = true;
                        //Don't grow if there's something on top of us
                        var intersecting = this.getIntersecting();
                        for (var i = 0; i < intersecting.length; i++)
                        {
                            if (!intersecting[i].treeType)
                            {
                                this.growthStage--;
                                this.solid = false;
                                break;
                            }
                        }
                    }
                    this.updateFrame();
                }
            }
        }
    },
    hurt: function(amt)
    {
        this.health -= amt;
        if (this.health <= 0)
        {
            this.health = gTreeHealth;
            this.growthStage--;
            if (this.growthStage <= 1)
            {
                this.solid = false;
            }
            this.updateFrame();
            if (this.growthStage < 0)
            {
                gGameState.setMap(this.mapX, this.mapY, null);
            }
        }
        return this.growthStage <= 1; //Passible if small
    },
    updateFrame: function()
    {
        this.frameX = (4 - this.growthStage) * 32;
    }
};
Object.setPrototypeOf(Tree.prototype, Sprite.prototype);

function Fireball(x, y, z, a, owner)
{
    Projectile.call(this, x, y, z, a, 2.0, owner);
    this.frameX = 0;
    this.frameY = 128;
    this.frameSize = 16;
    this.worldScale = this.frameSize * 0.5;
    this.timer = Math.random();
    this.anim = 0;
    this.speed = 64.0;
    this.life = 0.75;
}
Fireball.prototype = 
{
    update: function(deltaTime)
    {
        for (var i = 0; i < this.intersections.length; i++)
        {
            if (this.intersections[i].flammable && this.intersections[i] != this.owner)
            {
                if (this.intersections[i].canHurt) 
                {
                    var passible = this.intersections[i].hurt(0.50);
                    if (!passible) this.timer += 0.1;
                }
                this.intersections[i].onFire = true;
                this.intersections[i].fireTimer = this.intersections[i].fireSpeed;
            }
        }

        this.anim += deltaTime;
        if (this.anim > 0.05)
        {
            this.anim = 0;
            this.frameX += this.frameSize;
            if (this.frameX > 32) this.frameX = 0;
        }
        this.worldScale += deltaTime * 25.0;
        this.z += Math.sin(this.timer * 10.0) * -0.1;
        this.a += Math.cos(this.timer * 10.0) * 0.05;
        
        Projectile.prototype.update.call(this, deltaTime);
    }
};
Object.setPrototypeOf(Fireball.prototype, Projectile.prototype);

function Bone(x, y, z, a, owner)
{
    Projectile.call(this, x, y, z, a, 3.0, owner);
    this.frameX = 64;
    this.frameY = 128;
    this.frameSize = 16;
    this.worldScale = this.frameSize * 2.5;
    this.speed = 48.0;
    this.life = 5.0;
    this.frameSwitch = false;
    this.anim = 0;
}
Bone.prototype = 
{
    update: function(deltaTime)
    {
        for (var i = 0; i < this.intersections.length; i++)
        {
            if (this.intersections[i] == this.owner) continue;
            if (this.intersections[i].solid)
            {
                this.dead = true;
            }
            else if (this.intersections[i].canHurt)
            {
                var passible = this.intersections[i].hurt(15.0);
                this.dead = !passible;
            }
        }
        this.anim += deltaTime;
        if (this.anim > 0.1)
        {
            this.anim = 0;
            this.frameSwitch = !this.frameSwitch;
            if (this.frameSwitch)
                this.frameX = 80;
            else
                this.frameX = 64;
        }
        Projectile.prototype.update.call(this, deltaTime);
    }
};
Object.setPrototypeOf(Bone.prototype, Projectile.prototype);

function Twinkie(x, y)
{
    Item.call(this, x, y, 0, 4.0);
    this.z = 0;
    this.flashColor = "255, 255, 0";
    this.frameSize = 16;
    this.worldScale = this.frameSize * 2.5;
    this.frameX = 96;
    this.frameY = 128;
    this.flammable = false;
}
Twinkie.prototype = 
{
    update: function(deltaTime)
    {
        Item.prototype.update.call(this, deltaTime);
        this.z = Math.sin(gTimer * 5.0) * 2.0;
    },
    onCollect: function()
    {
        if (gGameState.player.health == 100) return;
        PlaySound("twinkie.wav", 0.5, false);
        gGameState.flashScreen(this.flashColor, 0.5, 0.5);
        gGameState.player.health = Math.min(100, gGameState.player.health + 20);
        gGameState.removeEnt(this);
    }
};
Object.setPrototypeOf(Twinkie.prototype, Item.prototype);

function Crystal(x, y, z)
{
    Item.call(this, x, y, z, 4);
    this.originZ = z;
    this.flashColor = "0,0,255";
    this.frameSize = 16;
    this.worldScale = this.frameSize * 2.5;
    this.frameX = 48;
    this.frameY = 128;
}
Crystal.prototype = 
{
    update: function(deltaTime)
    {
        Item.prototype.update.call(this, deltaTime);
        this.z = this.originZ + Math.sin(gTimer * 6.0 + this.x) * 2.0;
    },
    onCollect: function()
    {
        if (gGameState.player.crystals < MAX_CRYSTALS && gGameState.player.bending == false)
        {
            PlaySound("crystal.wav", 0.5, true);
            gGameState.flashScreen(this.flashColor, 0.5, 0.25);
            gGameState.player.crystals++;
            gGameState.removeEnt(this);
            if (gGameState.player.crystals >= MAX_CRYSTALS)
            {
                var key = "Q";
                switch(gGameState.player.control)
                {
                    case CTRL_TWINSTICK:
                        key = "Q";
                        break;
                    case CTRL_WOLFENSTEIN:
                        key = "C";
                        break;
                    case CTRL_WOLFENSTEIN2:
                        key = "L";
                        break;
                }
                gGameState.showMessage("PRESS " + key + " TO BEND REALITY", "#FF00FF", 5.0);
            }
        }
    }
};
Object.setPrototypeOf(Crystal.prototype, Item.prototype);

function Friend(x, y)
{
    Item.call(this, x, y, 0, 8.0);
    this.flashColor = "255, 255, 255";
    this.frameSize = 32;
    this.worldScale = this.frameSize * 2.5;
    this.frameX = 0;
    this.flammable = true;
    this.fireTimer = 0;
    this.fireSpeed = 3.0;
}
Friend.prototype = 
{
    friendType: true,
    update: function(deltaTime)
    {
        //Clear area to make us more visible
        for (var i = 0; i < gGameState.mapEntities.length; i++)
        {
            if (gGameState.mapEntities[i].treeType)
            {
                var dist = distance2DSqr(this.x, this.y, gGameState.mapEntities[i].x, gGameState.mapEntities[i].y);
                if (dist < 2304) //48 * 48
                {
                    gGameState.mapEntities[i].growthStage = 0;
                }
            }
        }
        Item.prototype.update.call(this, deltaTime);
        this.fireTimer -= deltaTime;
        if (this.fireTimer <= 0)
        {
            this.fireTimer = 0;
            this.onFire = false;
        }
    },
    onCollect: function()
    {
        gGameState.flashScreen(this.flashColor, 0.75);
        gGameState.friendsSaved++;
        gGameState.player.health = 100;
        //Add more skeletons to ramp up difficulty
        for (var i = 0; i < Math.floor(25 * gGameState.densityFactor); i++)
        {
            gGameState.spawnSkeletonSomewhere(true);
        }
        PlaySound(this.sound, 1.0, false);
        if (gGameState.friendsSaved >= 4)
        {
            gGameState.winTimer = 5.0;
            gGameState.fadeScreen("255,255,255", 5.0);
            gFadeSong = true;
        }
        gGameState.removeEnt(this);
    }
};
Object.setPrototypeOf(Friend.prototype, Item.prototype);