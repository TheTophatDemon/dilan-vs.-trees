function Entity(x, y, a, r)
{
    this.x = x;
    this.y = y;
    this.z = 0;
    this.a = a;
    this.r = r;
    this.solid = false;
}
Entity.prototype = 
{
    update: function(deltaTime)
    {

    },
    render2D: function(x, y)
    {

    },
    getIntersecting: function(solidsOnly, out)
    {
        out = out || [];
        var top = this.z - this.r;
        var bottom = this.z + this.r;
        for (var i = 0; i < gGameState.entities.length; i++)
        {
            var ent = gGameState.entities[i];
            if (ent != this && (!solidsOnly || ent.solid))
            {
                var dist = distance3DSqr(ent.x, ent.y, ent.z, this.x, this.y, this.z);
                if (dist < (this.r + ent.r) * (this.r + ent.r))
                {
                    out.push(ent);
                }
            }
        }
        //Check map
        var l = Math.floor((this.x - this.r) / gGameState.tileSize);
        var t = Math.floor((this.y - this.r) / gGameState.tileSize);
        var r = Math.floor((this.x + this.r) / gGameState.tileSize);
        var b = Math.floor((this.y + this.r) / gGameState.tileSize); 
        var left = Math.min(l, r);
        var right = Math.max(l, r);
        var top = Math.min(t, b);
        var bottom = Math.max(t, b);
        for (var i = left; i <= right; i++)
        {
            for (var j = top; j <= bottom; j++)
            {
                if (i < 0 || j < 0 || i >= gGameState.worldSize || j >= gGameState.worldSize) continue;
                var ent = gGameState.map[i][j];
                if (ent && ent != this)
                {
                    if (!solidsOnly || ent.solid)
                    {
                        var dist = distance3DSqr(ent.x, ent.y, ent.z, this.x, this.y, this.z);
                        if (dist < (this.r + ent.r) * (this.r + ent.r))
                        {
                            out.push(ent);
                        }
                    }
                }
            }
        }
        return out;
    },
    tryMove: function(dx, dy, entTypes)
    {
        var hit = false;
        if (entTypes)
        {
            for (var i = 0; i < gGameState.entities.length; i++)
            {
                if (gGameState.entities[i] == this) continue;
                for (var j = 0; j < entTypes.length; j++)
                {
                    if (gGameState.entities[i][entTypes[j]])
                    {
                        var ent = gGameState.entities[i];
                        var res = this.pushOut(this.x + dx, this.y + dy, this.z, this.r, ent.x, ent.y, ent.z, ent.r);
                        if (res)
                        {
                            hit = true;
                            dx += res[0];
                            dy += res[1];
                        }
                        break;
                    }
                }
            }
        }
        //Makes a rough rectangle of map coordinates to check for collisions in
        var l = Math.floor((this.x + dx - this.r * 4.0) / gGameState.tileSize);
        var t = Math.floor((this.y + dy - this.r * 4.0) / gGameState.tileSize);
        var r = Math.ceil((this.x + dx + this.r * 4.0) / gGameState.tileSize);
        var b = Math.ceil((this.y + dy + this.r * 4.0) / gGameState.tileSize); 
        var left = Math.min(l, r);
        var right = Math.max(l, r);
        var top = Math.min(t, b);
        var bottom = Math.max(t, b);
        for (var i = left; i <= right; i++)
        {
            for (var j = top; j <= bottom; j++)
            {
                if (i < 0 || j < 0 || i >= gGameState.worldSize || j >= gGameState.worldSize) continue;
                var ent = gGameState.map[i][j];
                if (ent && ent != this)
                {
                    if (!ent.solid) continue;
                    var res = this.pushOut(this.x + dx, this.y + dy, this.z, this.r, ent.x, ent.y, ent.z, ent.r);
                    if (res)
                    {
                        hit = true;
                        dx += res[0];
                        dy += res[1];
                    }
                }
            }
        }
        this.x += dx;
        this.y += dy;
        return hit;
    },
    pushOut: function(x1, y1, z1, r1, x2, y2, z2, r2)
    {
        if (z2 + r2 >= z1 - r1 && z2 - r2 <= z1 + r1) //Verticality test
        {
            var distSqr = distance2DSqr(x2, y2, x1, y1);
            if (distSqr < (r1 + r2) * (r1 + r2))
            {
                var dist = Math.sqrt(distSqr);
                var overlap = dist - r1 - r2;
                //Normalize the difference vector and then scale it by the overlap
                var diffX = x1 - x2;
                var diffY = y1 - y2;
                if (dist != 0)
                {
                    diffX /= dist;
                    diffY /= dist;
                }
                diffX *= -overlap;
                diffY *= -overlap;
                //Push the player away by that vector later
                return [diffX, diffY];
            }
        }
        return null;
    }
};

function Sprite(x, y, a, r)
{
    Entity.call(this, x, y, a, r);
    this.frameX = 0;
    this.frameY = 0;
    this.frameSize = 32;
    this.transformedCoords = null;
    this.worldScale = this.frameSize;
    this.flammable = false;
    this.onFire = false;
}
Sprite.prototype = 
{
    spriteType: true, //If an object has this property, it has a prototype derived from Sprite
    transformAndClip: function(player)
    {
        var transformed = player.toCameraSpace(this.x, this.y, this.z);
        player.toClipSpace(transformed[0], transformed[1], transformed[2], transformed);
        if (transformed[1] > 0 && transformed[1] < CAMERA_FAR)
        {
            transformed[0] /= transformed[3];
            transformed[2] /= transformed[3];
            transformed[4] = 0.0015 * player.camRatio * this.worldScale / transformed[3];
            if (transformed[0] > -1.0 && transformed[0] < 1.0)
            {
                this.transformedCoords = transformed;
                return true; //True if visible
            }
        }
        return false;
    },
    render2D: function(x, y)
    {
        gContext.drawImage(gSpritesImg, this.frameX, this.frameY, this.frameSize, this.frameSize, x - this.r, y - this.r, this.r * 2.0, this.r * 2.0);
    },
    render3D: function()
    {
        var scaleFactor = this.transformedCoords[4] * gHeight;
        var halfScale = scaleFactor / 2.0;
        
        if (this.onFire)
        {
            gContext.drawImage(gSpritesImg, 32 * gFlameFrame, 144, 32, 32, 
                gWidth * this.transformedCoords[0] - halfScale, -halfScale + this.transformedCoords[2] * gHeight, scaleFactor, scaleFactor);
        }
        gContext.drawImage(gSpritesImg, this.frameX, this.frameY, this.frameSize, this.frameSize, 
            gWidth * this.transformedCoords[0] - halfScale, -halfScale + this.transformedCoords[2] * gHeight, scaleFactor, scaleFactor);
    }
};
Object.setPrototypeOf(Sprite.prototype, Entity.prototype);

function Item(x, y, z, r)
{
    Sprite.call(this, x, y, 0.0, r);
    this.z = z;
}
Item.prototype = 
{
    update: function(deltaTime)
    {
        var dist = distance2D(this.x, this.y, gGameState.player.x, gGameState.player.y);
        if (dist < this.r + gGameState.player.r)
        {
            this.onCollect();
        }
    },
    onCollect: function()
    {
        gGameState.flashScreen("255,255,255", 0.25);
        gGameState.removeEnt(this);
    }
};
Object.setPrototypeOf(Item.prototype, Sprite.prototype);

const ENEMY_WANDER = 0;
const ENEMY_DEAD = 1;
function Enemy(x, y, a, r)
{
    Sprite.call(this, x, y, a, r);
    this.health = 100;
    this.state = -1;
    this.animation = [0];
    this.animTimer = 0;
    this.animFrame = 0;
    this.animSpeed = 0.1;
    this.animLoop = true;
    this.stateTimer = 0.0;
}
Enemy.prototype = 
{
    canHurt: true,
    enemyType: true,
    update: function(deltaTime)
    {
        this.animTimer += deltaTime;
        if (this.animTimer > this.animSpeed)
        {
            this.animTimer = 0;
            this.animFrame++;
            if (this.animFrame >= this.animation.length)
            {
                if (this.animLoop)
                    this.animFrame = 0;
                else
                    this.animFrame = this.animation.length - 1;
            }
            this.frameX = this.animation[this.animFrame] * this.frameSize;
        }
        this.stateTimer += deltaTime;
    },
    hurt: function(amt)
    {
        this.health -= amt;
        if (this.health <= 0)
        {
            this.changeState(ENEMY_DEAD);
        }
        return false; //impassible
    },
    changeState: function(newState)
    {
        if (this.state != newState)
        {
            this.enterState(newState);
            this.leaveState(this.state);
            this.stateTimer = 0.0;
        }
        this.state = newState;
    },
    enterState: function(newState)
    {

    },
    leaveState: function(oldState)
    {

    },
    setAnim: function(newAnim, speed, loop)
    {
        this.animation = newAnim;
        this.animTimer = 0;
        this.animFrame = 0;
        this.animSpeed = speed;
        this.animLoop = loop;
    }
};
Object.setPrototypeOf(Enemy.prototype, Sprite.prototype);

function Projectile(x, y, z, a, r, owner)
{
    Sprite.call(this, x, y, a, r);
    this.z = z;
    this.owner = owner;
    this.timer = 0.0;
    this.life = 1.0;
    this.speed = 64.0;
    this.intersections = [];
    this.dead = false;
}
Projectile.prototype = 
{
    update: function(deltaTime)
    {
        this.timer += deltaTime;
        if (this.timer > this.life)
        {
            this.dead = true;
        }
        this.x += Math.cos(this.a) * this.speed * deltaTime;
        this.y += Math.sin(this.a) * this.speed * deltaTime;
        this.getIntersecting(false, this.intersections);
        if (this.dead)
        {
            gGameState.removeEnt(this);
        }
    }
};
Object.setPrototypeOf(Projectile.prototype, Sprite.prototype);