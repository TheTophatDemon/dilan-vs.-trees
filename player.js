const PLAYER_MAX_SPEED = 96.0;
const PLAYER_TURN_SPEED = Math.PI;
const CAMERA_FAR = 384.0;
const CAMERA_NEAR = 1.0;
const CAMERA_LENGTH = CAMERA_FAR - CAMERA_NEAR;
const CTRL_TWINSTICK = 0;
const CTRL_WOLFENSTEIN = 1;
const CTRL_WOLFENSTEIN2 = 2;
const PLAYER_DEF = 0;
const PLAYER_DEAD = 1;
const MAX_CRYSTALS = 25;
function Player(x, y, a, r, fov, control)
{
    Entity.call(this, x, y, a, r);
    this.z = 0.0;
    this.fwd = 0.0;
    this.stf = 0.0;
    this.cosa = Math.cos(a);
    this.sina = Math.sin(a);
    this.icosa = Math.cos(-a);
    this.isina = Math.sin(-a);
    this.noclip = false;
    this.walk = 0;
    this.fireTimer = 0;
    this.fireSoundTimer = 0;
    this.health = 100;
    this.state = PLAYER_DEF;
    this.crystals = 0;
    this.bending = false;
    this.fov = fov;
    this.calculateFrustum();
    this.control = control;
    this.updateControl();
}
Player.prototype = 
{
    canHurt: true,
    playerType: true,
    update: function(deltaTime)
    {  
        if (this.state == PLAYER_DEF)
        {
            if (gDebugMode)
            {
                //Cheaty cheats
                if (gKeyHits[106]) this.noclip = !this.noclip; //Numpad *
                if (gKeyHits[96]) //Numpad 0
                {
                    var fr = new Friend(this.x + this.cosa * 20.0, this.y + this.sina * 20.0);
                    fr.frameY = 176;
                    fr.sound = "ray.wav";
                    gGameState.addEnt(fr);
                }
                if (gKeyHits[109]) this.hurt(50); //Numpad -
                if (gKeyHits[107]) this.health = 100; //Numpad +
                if (gKeyHits[105]) this.crystals+=5; //Numpad 9
                if (gKeyStatus[98]) //Numpad 2
                {
                    for (var i = 0; i < gGameState.mapEntities.length; i++)
                    {
                        if (gGameState.mapEntities[i].treeType)
                        {
                            gGameState.mapEntities[i].growthStage = 0;
                            gGameState.mapEntities[i].solid = false;
                            gGameState.mapEntities[i].updateFrame();
                        }
                    }
                }
                if (gKeyHits[100]) //Numpad 4
                {
                    this.fov -= 10;
                    this.calculateFrustum();
                }
                else if (gKeyHits[102]) //Numpad 6
                {
                    this.fov += 10;
                    this.calculateFrustum();
                }
            }

            if (this.crystals >= MAX_CRYSTALS && gKeyHits[this.bendKey] && !this.bending)
            {
                this.bending = true;
                SetSong(gBendMusic, false);
            }
            if (this.bending)
            {
                this.crystals -= deltaTime;
                if (this.crystals <= 0.0)
                {
                    this.crystals = 0;
                    this.bending = false;
                }
                for (var i = 0; i < gGameState.mapEntities.length; i++)
                {
                    if (gGameState.mapEntities[i].treeType)
                    {
                        var tree = gGameState.mapEntities[i];
                        var sqDist = distance2DSqr(tree.x, tree.y, this.x, this.y);
                        if (sqDist < 9216.0) 
                        {
                            tree.newZ = -32.0;
                        }
                    }
                }
            }

            if (gKeyStatus[this.forwardKey])
            {
                this.fwd = PLAYER_MAX_SPEED;
                this.walk += deltaTime;
            } 
            else if (gKeyStatus[this.backKey])
            {
                this.fwd = -PLAYER_MAX_SPEED;
                this.walk -= deltaTime;
            }
            else
            {
                this.fwd = 0.0;
            }
            
            if (this.control == CTRL_TWINSTICK || gKeyStatus[this.strafeKey])
            {
                if (gKeyStatus[this.strafeLeftKey])
                {
                    this.stf = -PLAYER_MAX_SPEED;
                    this.walk += deltaTime * (Math.sign(this.fwd) || 1.0);
                }
                else if (gKeyStatus[this.strafeRightKey])
                {
                    this.stf = PLAYER_MAX_SPEED;
                    this.walk += deltaTime * (Math.sign(this.fwd) || 1.0);
                }
                else
                {
                    this.stf = 0.0;
                }
            }
            else
            {
                this.stf = 0.0;
            }

            if (this.control == CTRL_TWINSTICK || !gKeyStatus[this.strafeKey])
            {
                if (gKeyStatus[this.turnLeftKey])
                {
                    this.a -= PLAYER_TURN_SPEED * deltaTime;
                }
                else if (gKeyStatus[this.turnRightKey])
                {
                    this.a += PLAYER_TURN_SPEED * deltaTime;
                }
            }

            if (gDebugMode && gKeyStatus[110]) //numpad period
            {
                this.fwd = PLAYER_MAX_SPEED * 4.0;
                this.walk += deltaTime * 4.0;
            }

            if (gKeyStatus[this.fireKey])
            {
                this.fireTimer += deltaTime;
                if (this.fireTimer > 0.01)
                {
                    this.fireTimer = 0.0;
                    gGameState.addEnt(new Fireball(this.x + this.cosa * this.r * 2.0 / this.camWidth, 
                        this.y + this.sina * this.r * 2.0 / this.camWidth, 4.0, this.a, this));
                }
                this.fireSoundTimer += deltaTime;
                if (this.fireSoundTimer > 0.1)
                {
                    this.fireSoundTimer = 0;
                    PlaySound("flamethrower.wav", 0.5, true);
                }
            }
            else
            {
                this.fireTimer = 0;
                this.fireSoundTimer = 0;
            }

            this.cosa = Math.cos(this.a);
            this.sina = Math.sin(this.a);
            this.icosa = Math.cos(-this.a);
            this.isina = Math.sin(-this.a);
            
            var dx = this.cosa * this.fwd * deltaTime - this.sina * this.stf * deltaTime;
            var dy = this.sina * this.fwd * deltaTime + this.cosa * this.stf * deltaTime;
            if (this.noclip)
            {
                this.x += dx;
                this.y += dy;
            }
            else
            {
                this.tryMove(dx, dy);
            }
        }
        else if (this.state == PLAYER_DEAD)
        {
            this.z += deltaTime * 8.0;
            if (this.z > 14.0) this.z = 14.0;
        }
    },
    hurt: function(amt)
    {
        if (this.state != PLAYER_DEAD)
        {
            this.health -= amt * gGameSetupState.options[OPT_DIFF].value;
            gGameState.flashScreen("255,0,0", 0.4);
            if (this.health <= 0)
            {
                this.health = 0;
                this.state = PLAYER_DEAD;
                gGameState.fadeScreen("255, 0, 0", 3.0);
                gFadeSong = true;
                gGameState.loseTimer = 3.0;
                PlaySound("dilan_death_" + Math.floor(Math.random() * 2.0) + ".wav");
            }
            else
            {
                PlaySound("dilan_hurt_" + Math.floor(Math.random() * 4.0) + ".wav");
            }
        }
        return false; //impassible
    },
    render2D: function(x, y)
    {
        gContext.fillStyle = "#FF0000";
        gContext.beginPath();
        gContext.arc(x, y, this.r, 0.0, TWOPI, false);
        gContext.fill();
        gContext.strokeStyle = "#AA0000";
        gContext.beginPath();
        gContext.moveTo(x, y);
        gContext.lineTo(x + this.cosa * this.r * 2.0,
            y + this.sina * this.r * 2.0);
        gContext.stroke();
    },
    render3D: function(player)
    {
        //Nope
    },
    calculateFrustum: function()
    {
        var radFOV = this.fov * Math.PI / 180.0;
        this.camWidth = gAspectRatio * Math.sin(radFOV / 2.0) * CAMERA_FAR / 64.0;
        this.camRatio = CAMERA_LENGTH / this.camWidth;
    },
    updateControl: function()
    {
        switch (this.control)
        {
            case CTRL_TWINSTICK:
                this.forwardKey = 87; //w
                this.backKey = 83; //s
                this.strafeLeftKey = 65; //a
                this.strafeRightKey = 68; //d
                this.turnLeftKey = 37; //left
                this.turnRightKey = 39; //right
                this.fireKey = 32; //space
                this.bendKey = 81; //q
                this.strafeKey = 0; //doesn't matter what this is, we don't need it
                break;
            case CTRL_WOLFENSTEIN:
                this.forwardKey = 38; //up
                this.backKey = 40; //down
                this.strafeKey = 88; //x
                this.turnLeftKey = 37; //left
                this.strafeLeftKey = 37;
                this.turnRightKey = 39; //right
                this.strafeRightKey = 39;
                this.fireKey = 90; //z
                this.bendKey = 67; //c
                break;
            case CTRL_WOLFENSTEIN2:
                this.forwardKey = 87; //w
                this.backKey = 83; //s
                this.strafeKey = 74; //j
                this.turnLeftKey = 65; //a
                this.strafeLeftKey = 65;
                this.turnRightKey = 68; //d
                this.strafeRightKey = 68;
                this.fireKey = 75; //k
                this.bendKey = 76; //l
                break;
        }
    },
    toCameraSpace: function(wx, wy, wz, out)
    {
        /*
        [-sina cosa 0]   [x]   [x * -sina + y * cosa]
        [cosa sina  0] * [y] = [x * cosa + y * sina]
        [0    0     1]   [1]   [0 + 0 + 1]
        */
       wx -= this.x;
       wy -= this.y;
       wz -= this.z;
       out = out || [];
       out[0] = wx * this.isina + wy * this.icosa;
       out[1] = wx * -this.icosa + wy * this.isina;
       out[2] = wz; //No pitch rotation, so no need to transform z
       return out;
    },
    toClipSpace: function(cx, cy, cz, out)
    {
        out = out || [];
        out[0] = CAMERA_NEAR * cx / this.camWidth;
        out[1] = -CAMERA_FAR * (cy + CAMERA_NEAR) / CAMERA_LENGTH;
        out[2] = CAMERA_NEAR * cz / (this.camWidth / gAspectRatio);
        out[3] = -cy; //We need an unscaled depth value for more accurate sorting.
        return out;
    }
}
Object.setPrototypeOf(Player.prototype, Entity.prototype);