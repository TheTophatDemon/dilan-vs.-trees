//TODO:
//test in newgrounds, gamejolt, itchio

const TWOPI = Math.PI * 2.0;
const HALFPI = Math.PI / 2.0;

var gSpritesImg = null;
var gAssetsLoaded = 0;

var gWidth = 640;
var gHalfWidth = gWidth / 2.0;
var gHeight = 480;
var gHalfHeight = gHeight / 2.0;
var gAspectRatio = gWidth / gHeight;

var gCanvas = null;
var gContext = null;
var gRequestId = 0;

var gLastTime = 0;
var gFPSTicks = 0;
var gFPS = 0;
var gFPSTimer = 0;

var gTimer = 0;
var gKeyStatus = new Array();
var gKeyHits = new Array();
var gDebugMode = false;

var gState = null;
var gGameState = null;
var gTitleState = null;
var gWinState = null;
var gLoseState = null;
var gTutorState = null;
var gGameSetupState = null;

function log(msg)
{
    console.log(msg);
}

window.onload = function()
{
    window.addEventListener("keydown", function(e){
        switch(e.keyCode){
            case 37:
            case 39: 
            case 38:  
            case 40:
            case 32: 
                e.preventDefault(); 
                break;
            default: 
                break;
        }
        gKeyStatus[e.keyCode] = true;
        gKeyHits[e.keyCode] = true;
    }, false);
    window.addEventListener("keyup", function(e){
        switch(e.keyCode){
            case 37:
            case 39: 
            case 38:  
            case 40:
            case 32: 
                e.preventDefault(); 
                break;
            default: 
                break;
        }
        gKeyStatus[e.keyCode] = false;
    }, false);
    gCanvas = document.getElementById("canvas");
    gContext = gCanvas.getContext("2d");
    gContext.imageSmoothingEnabled = false;
    gContext.msImageSmoothingEnabled = false;
    gContext.webkitImageSmoothingEnabled = false;

    gLastTime = Date.now() / 1000.0;
    init();
    tick();
}

function init()
{
    gSpritesImg = new Image();
    gSpritesImg.src = "sprites.png";
    gSpritesImg.onload = function() {gAssetsLoaded++;}

    gTitleState = new TitleState();
    gWinState = new WinState();
    gLoseState = new LoseState();
    gGameSetupState = new GameSetupState();
    gTutorState = new TutorState(); //The order matters

    ChangeState(gTitleState);
}

function tick()
{
    var now = Date.now() / 1000.0;
    var deltaTime = now - gLastTime;
    gLastTime = now;

    gTimer += deltaTime;
    gFPSTimer += deltaTime;
    if (gFPSTimer > 1.0)
    {
        gFPSTimer = 0;
        gFPS = gFPSTicks;
        gFPSTicks = 0;
    }
    else
    {
        gFPSTicks++;
    }

    gContext.setTransform(1, 0, 0, 1, 0, 0);
    gContext.clearRect(0, 0, gWidth, gHeight);
    
    if (gState)
    {
        gState.update(deltaTime);
        if (gAssetsLoaded > 0)
        {
            gState.render(deltaTime);
        }
    }
    UpdateAudio(deltaTime);

    for (var i = 0; i < gKeyHits.length; i++)
    {
        gKeyHits[i] = false;
    }
    gRequestId = window.requestAnimationFrame(tick);
}

function ChangeState(newState)
{
    if (newState != gState)
    {
        if (gState) gState.leave();
        if (newState) newState.enter();
    }
    gState = newState;
}

const RENDER_2D = 0;
const RENDER_2D_TRANS = 1;
const RENDER_3D = 2;
var gForestSpeed = 0.05; //Number of seconds it takes to spawn one additional tree
var gFlameFrame = 0;
var gFlameAnim = 0;
function GameState(worldSize, fov, control)
{
    this.worldSize = worldSize || 64;
    this.densityFactor = (this.worldSize / 64.0) * (this.worldSize / 64.0);
    this.tileSize = 32;
    this.entities = new Array();
    this.mapEntities = new Array();
    this.renderMode = RENDER_3D;
    this.zoom = 1;

    this.player = new Player(this.tileSize * this.worldSize / 2.0 + this.tileSize / 2.0, 
        this.tileSize * this.worldSize / 2.0 + this.tileSize / 2.0, 0, 6, fov || 90.0, control || CTRL_TWINSTICK);
    this.addEnt(this.player);

    this.weaponAnim = new Animation(0, 64, 64, [1, 2], 0.1);

    this.generateMap();
    this.forestTimer = 0;

    this.flashColor = "255,255,255";
    this.flashSpeed = 0;
    this.flashTimer = 0;
    this.flashAlpha = 1.0;

    this.fadeColor = "0, 0, 0";
    this.fadeSpeed = 0;
    this.fadeTimer = 0;

    this.messageTimer = 0;
    this.message = "";
    this.messageColor = "#FFFFFF";

    this.winTimer = 0;
    this.loseTimer = 0.0;
    this.friendsSaved = 0;

    this.skyGradient = gContext.createLinearGradient(0, 0, 0, gHalfHeight);
    this.skyGradient.addColorStop(0, "#000080");
    this.skyGradient.addColorStop(1.0, "#0000FF");
    this.grassGradient = gContext.createLinearGradient(0, gHalfHeight, 0, gHeight);
    this.grassGradient.addColorStop(0, "#008000");
    this.grassGradient.addColorStop(1, "#00FF00");

    SetSong(gGameMusic, true);
}
GameState.prototype = 
{
    generateMap: function()
    {

        this.map = new Array();
        for (var i = 0; i < this.worldSize; i++)
        {
            this.map[i] = new Array();
            for (var j = 0; j < this.worldSize; j++)
            {
                this.setMap(i, j, null);
                if (distance2D(i, j, this.worldSize / 2.0, this.worldSize / 2.0) <= 2.0)
                {
                    //Keep this area empty. It's where the player spawns
                }
                else if (i == 0 || j == 0 || i == this.worldSize - 1 || j == this.worldSize - 1)
                {
                    var fence = new Sprite(i * this.tileSize + this.tileSize / 2.0, j * this.tileSize + this.tileSize / 2.0, 0, 16.0);
                    fence.solid = true;
                    fence.worldScale = fence.frameSize * 2.5;
                    fence.frameX = 0;
                    fence.frameY = 32;
                    this.setMap(i, j, fence);
                }
                else if (Math.random() < 0.8)
                {
                    this.setMap(i, j, new Tree(i * this.tileSize + Math.random() * this.tileSize, j * this.tileSize + Math.random() * this.tileSize, 3));
                    this.map[i][j].growthTimer = Math.random() * gGrowthSpeed;
                }
                else if (Math.random() < 0.2)
                {
                    var stone = new Sprite(i * this.tileSize, j * this.tileSize, 0, 8);
                    stone.solid = true;
                    stone.worldScale = stone.frameSize * 2.5;
                    stone.frameX = 32;
                    stone.frameY = 32;
                    this.setMap(i, j, stone);
                }
            }
        }
        //Spawn beds of grass & flowers
        for (var i = 0; i < Math.floor(38 * this.densityFactor); i++)
        {
            var mx = Math.floor(Math.random() * (this.worldSize - 4)) + 2;
            var my = Math.floor(Math.random() * (this.worldSize - 4)) + 2;
            var count = Math.floor(Math.random() * 12);
            for (var j = 0; j < count; j++)
            {
                var spr = new Sprite(mx * this.tileSize + this.tileSize / 2.0, my * this.tileSize + this.tileSize / 2.0, 0, 4);
                spr.worldScale = spr.frameSize * 2.5;
                spr.frameY = 32;
                spr.flammable = true;
                spr.fireTimer = 0.0;
                spr.fireSpeed = 1.0;
                if (Math.random() < 0.2)
                {
                    spr.frameX = 96;
                }
                else
                {
                    spr.frameX = 64;
                }
                spr.update = function(deltaTime)
                {
                    if (this.onFire)
                    {
                        this.fireTimer -= deltaTime;
                        if (this.fireTimer <= 0)
                        {
                            this.fireTimer = 0;
                            gGameState.setMap(this.mapX, this.mapY, null);
                        }
                    }
                }
                this.setMap(mx, my, spr);
                if (Math.random() > 0.5)
                    mx++;
                else
                    mx--;
                if (Math.random() > 0.5)
                    my++;
                else
                    my--;
                if (mx <= 0 || my <= 0 || mx >= this.worldSize - 1 || my >= this.worldSize - 1)
                {
                    break;
                }
            }
        }
        //Spawn friends
        var baseAngle = Math.random() * TWOPI;
        //They are all spread out 90 degrees from eachother from the center of the map
        for (var k = 0; k < 4; k++)
        {
            var angle = baseAngle + (k * HALFPI);
            var magOffs = Math.random() * 5.0;
            var xx = Math.cos(angle) * (this.worldSize - 5.0 - magOffs) / 2.0;
            var yy = Math.sin(angle) * (this.worldSize - 5.0 - magOffs) / 2.0;
            var mx = Math.floor(this.worldSize / 2.0 + xx);
            var my = Math.floor(this.worldSize / 2.0 + yy);
            for (var i = -1; i < 2; i++)
            {
                for (var j = -1; j < 2; j++)
                {
                    this.setMap(mx + i, my + j, null);
                }
            }
            var fr = new Friend(mx * this.tileSize + this.tileSize / 2.0, my * this.tileSize + this.tileSize / 2.0);
            fr.frameY = 176 + k * fr.frameSize;
            switch(k)
            {
                case 0: //Ray
                    fr.flashColor = "200, 64, 64";
                    fr.sound = "ray.wav";
                    break;
                case 1: //Rastera
                    fr.flashColor = "0, 255, 0";
                    fr.sound = "rastera.wav";
                    break;
                case 2: //Bird Man
                    fr.flashColor = "0, 0, 255";
                    fr.sound = "birdman.wav";
                    break;
                case 3: //The President
                    fr.flashColor = "128,64,64";
                    fr.sound = "president.wav";
                    break;
            }
            this.addEnt(fr);
        }
        //Spawn skeletons
        for (var k = 0; k < Math.floor(35 * this.densityFactor); k++)
        {
            this.spawnSkeletonSomewhere();
        }
        //Twinkies
        for (var k = 0; k < Math.floor(16 * this.densityFactor); k++)
        {
            var mx = Math.floor(Math.random() * (this.worldSize - 4)) + 2;
            var my = Math.floor(Math.random() * (this.worldSize - 4)) + 2;
            this.setMap(mx, my, null);
            this.addEnt(new Twinkie(mx * this.tileSize + this.tileSize/ 2.0, my * this.tileSize + this.tileSize / 2.0));
        }
        //Crystals
        for (var k = 0; k < Math.floor(20 * this.densityFactor); k++)
        {
            var mx = Math.floor(Math.random() * (this.worldSize - 8)) + 4;
            var my = Math.floor(Math.random() * (this.worldSize - 8)) + 4;
            this.setMap(mx, my, null);
            var cry = new Crystal(mx * this.tileSize + this.tileSize / 2.0, my * this.tileSize + this.tileSize / 2.0, 8);
            this.addEnt(cry);
            if (Math.random() < 0.5) 
            { this.setMap(mx + 1, my, null); this.addEnt(new Crystal(cry.x + this.tileSize, cry.y, 8)); }
            if (Math.random() < 0.5) 
            { this.setMap(mx - 1, my, null); this.addEnt(new Crystal(cry.x - this.tileSize, cry.y, 8)); } 
            if (Math.random() < 0.5) 
            { this.setMap(mx, my - 1, null); this.addEnt(new Crystal(cry.x, cry.y - this.tileSize, 8)); }
            if (Math.random() < 0.5) 
            { this.setMap(mx, my + 1, null); this.addEnt(new Crystal(cry.x, cry.y + this.tileSize, 8)); }
        }
    },
    update: function(deltaTime)
    {
        if (gKeyHits[13] || gKeyHits[27]) //enter || escape
        {
            ChangeState(gTitleState);
        }

        if (gKeyHits[99] && gDebugMode) //Numpad 3 
        {
            this.renderMode = (this.renderMode + 1) % 3;
        }

        gFlameAnim += deltaTime;
        if (gFlameAnim > 0.1)
        {
            gFlameAnim = 0;
            gFlameFrame++;
            if (gFlameFrame > 2) gFlameFrame = 0;
        }

        if (this.winTimer > 0)
        {
            this.winTimer -= deltaTime;
            if (this.winTimer <= 0)
            {
                PlaySound("dilan_win.wav");
                this.winTimer = 0;
                ChangeState(gWinState);
            }
        }
        if (this.loseTimer > 0)
        {
            this.loseTimer -= deltaTime;
            if (this.loseTimer <= 0)
            {
                this.loseTimer = 0;
                ChangeState(gLoseState);
            }
        }

        for (var i = 0; i < this.entities.length; i++)
        {
            this.entities[i].update(deltaTime);
        }
        for (var i = 0; i < this.mapEntities.length; i++)
        {
            this.mapEntities[i].update(deltaTime);
        }

        this.forestTimer += deltaTime;
        if (this.forestTimer > gForestSpeed)
        {
            this.forestTimer = 0.0;
            var tries = 0;
            while (tries < 100)
            {
                var x = Math.floor(Math.random() * (this.worldSize - 2)) + 1;
                var y = Math.floor(Math.random() * (this.worldSize - 2)) + 1;
                if (this.map[x][y])
                {
                    if (this.map[x][y].treeType)
                    {
                        foundSpot = true;
                        var leftN = this.map[x-1][y];
                        var rightN = this.map[x+1][y];
                        var topN = this.map[x][y-1];
                        var bottomN = this.map[x][y+1];
                        if (!leftN) {
                            this.setMap(x-1, y, new Tree(this.map[x][y].x - this.tileSize, this.map[x][y].y, 0)); 
                            break;
                        }
                        else if (!rightN) {
                            this.setMap(x+1, y, new Tree(this.map[x][y].x + this.tileSize, this.map[x][y].y, 0)); 
                            break;
                        }
                        else if (!topN) {
                            this.setMap(x, y-1, new Tree(this.map[x][y].x, this.map[x][y].y - this.tileSize, 0)); 
                            break;
                        }
                        else if (!bottomN) {
                            this.setMap(x, y+1, new Tree(this.map[x][y].x, this.map[x][y].y + this.tileSize, 0)); 
                            break;
                        }
                    }
                }
                tries++;          
            }
        }
    },
    render: function(deltaTime)
    {
        if (this.renderMode == RENDER_2D)
        {
            if (gKeyStatus[103]) //Np 7
            {
                this.zoom -= deltaTime;
            }
            else if (gKeyStatus[100]) //Np 4
            {
                this.zoom += deltaTime;
            }

            gContext.translate(gHalfWidth, gHalfHeight);
            gContext.scale(this.zoom, this.zoom);
            gContext.translate(-this.player.x, -this.player.y);
            //gContext.translate(gHalfWidth / this.zoom, gHalfHeight / this.zoom);
            for (var i = 0; i < this.entities.length; i++)
            {
                this.entities[i].render2D(this.entities[i].x, this.entities[i].y);
            }
            for (var i = 0; i < this.mapEntities.length; i++)
            {
                this.mapEntities[i].render2D(this.mapEntities[i].x, this.mapEntities[i].y);
            }
        }
        else if (this.renderMode == RENDER_2D_TRANS)
        {
            gContext.translate(gHalfWidth, gHalfHeight);
            for (var i = 0; i < this.entities.length; i++)
            {
                var transformed = this.player.toCameraSpace(this.entities[i].x, this.entities[i].y, this.entities[i].z);
                this.entities[i].render2D(transformed[0], transformed[1]);
            }
            for (var i = 0; i < this.mapEntities.length; i++)
            {
                var transformed = this.player.toCameraSpace(this.mapEntities[i].x, this.mapEntities[i].y, this.mapEntities[i].z);
                this.mapEntities[i].render2D(transformed[0], transformed[1]);
            }
        }
        else if (this.renderMode == RENDER_3D)
        {
            //Sky and ground
            gContext.fillStyle = this.skyGradient;
            gContext.fillRect(0, -this.player.z * CAMERA_NEAR, gWidth, gHalfHeight);
            gContext.fillStyle = this.grassGradient;
            gContext.fillRect(0, -this.player.z * CAMERA_NEAR + gHalfHeight, gWidth, gHeight);
            //Sprites
            gContext.translate(gHalfWidth, gHalfHeight);
            var visibleSprites = new Array();
            for (var i = 0; i < this.entities.length; i++)
            {
                if (this.entities[i].spriteType)
                {
                    var visible = this.entities[i].transformAndClip(this.player);
                    if (visible)
                    {
                        visibleSprites.push(this.entities[i]);
                    }
                }
            }
            for (var i = 0; i < this.mapEntities.length; i++)
            {
                if (this.mapEntities[i].spriteType)
                {
                    var visible = this.mapEntities[i].transformAndClip(this.player);
                    if (visible)
                    {
                        visibleSprites.push(this.mapEntities[i]);
                    }
                }
            }
            visibleSprites.sort(function(a, b) {
                return b.transformedCoords[3] - a.transformedCoords[3];
            });
            for (var i = 0; i < visibleSprites.length; i++)
            {
                visibleSprites[i].render3D();
            }
            //HUD
            gContext.translate(-gHalfWidth, -gHalfHeight);
            gContext.font = "12pt arial";
            gContext.textAlign = "left";
            gContext.fillStyle = "#FFFFFF";
            if (gKeyHits[97]) gDebugMode = !gDebugMode; //numpad 1
            if (gDebugMode)
            {
                gContext.fillText("Sprites: " + visibleSprites.length, 0, 12.0);
                gContext.fillText("FPS: " + gFPS, 0, 32.0);
                var enmCount = 0;
                for (var i = 0; i < this.entities.length; i++)
                {
                    if (this.entities[i].enemyType)
                    {
                        enmCount++;
                    }
                }
                gContext.fillText("Enemies: " + enmCount, 0, 52);
            }
            //Weapon
            var weaponX = gHalfWidth - 128 + Math.sin(this.player.walk * 2.0) * 8.0;
            var weaponY = gHeight - 224 + Math.cos(this.player.walk * 4.0) * 16.0 + (this.loseTimer == 0 ? 0 : (5.0 - this.loseTimer) * 256);
            if (gKeyStatus[this.player.fireKey])
            {
                this.weaponAnim.update(deltaTime);
                gContext.drawImage(gSpritesImg, this.weaponAnim.frameX, this.weaponAnim.frameY, 64, 64, weaponX, weaponY, 256, 256);
            }
            else
            {
                gContext.drawImage(gSpritesImg, 0, 64, 64, 64, weaponX, weaponY, 256, 256);
            }
            //Stats
            gContext.fillStyle="rgba(0, 0, 0, 0.75)";
            gContext.fillRect(0, gHeight - 64, 192, 64);
            gContext.font = "16pt bold helvetica";
            gContext.fillStyle = "#FF0000";
            gContext.fillText("HEALTH: " + this.player.health, 4, gHeight - 48);
            gContext.fillStyle = "#00A0FF";
            gContext.fillText("CRYSTALS: " + Math.floor(this.player.crystals) + "/" + MAX_CRYSTALS, 4, gHeight - 32);
            gContext.fillStyle = "#00FF00";
            gContext.fillText("FRIENDS: " + this.friendsSaved + "/4", 4, gHeight - 16);
            //Message
            if (this.messageTimer > 0)
            {
                this.messageTimer -= deltaTime;
                if (this.messageTimer <= 0) this.messageTimer = 0;
                gContext.fillStyle = this.messageColor;
                gContext.font = "24pt bold helvetica";
                gContext.textAlign = "center";
                gContext.fillText(this.message, gHalfWidth, gHalfHeight);
            }
            gContext.textAlign = "left";
            //Screen flash
            this.flashTimer += deltaTime;
            if (this.flashTimer < this.flashSpeed && this.flashSpeed != 0.0) 
            {
                var alpha = (1.0 - this.flashTimer / this.flashSpeed) * this.flashAlpha;
                gContext.fillStyle = "rgba(" + this.flashColor + "," + alpha + ")";
                gContext.fillRect(0, 0, gWidth, gHeight);
            }
            if (this.player.bending)
            {
                gContext.fillStyle = "rgba(0, 0, 255, 0.25)";
                gContext.fillRect(0, 0, gWidth, gHeight);
            }
            //Screen fade
            this.fadeTimer += deltaTime;
            if (this.fadeSpeed != 0.0)
            {
                var alpha = this.fadeTimer / this.fadeSpeed;
                gContext.fillStyle = "rgba(" + this.fadeColor + "," + alpha + ")";
                gContext.fillRect(0, 0, gWidth, gHeight);
            }
        }
    },
    enter: function()
    {
        this.player.fov = gGameSetupState.options[1].value;
        this.player.control = gGameSetupState.options[2].value;
        this.player.calculateFrustum();
        this.player.updateControl();
    },
    leave: function()
    {
        
    },
    setMap: function(x, y, e)
    {
        if (this.map[x][y]) 
        {
            this.map[x][y].onMap = false;
            var idx = this.mapEntities.indexOf(this.map[x][y]);
            if(idx >= 0) this.mapEntities.splice(idx, 1);
        }
        this.map[x][y] = e;
        if (e)
        {
            e.onMap = true;
            e.mapX = x;
            e.mapY = y;
            this.mapEntities.push(e);
        }
    },
    addEnt: function(e)
    {
        this.entities.push(e);
    },
    removeEnt: function(e)
    {
        var idx = this.entities.indexOf(e);
        if (idx >= 0)
        {
            this.entities.splice(idx, 1);
        }
        else
        {
            log("Trying to delete entity that isn't on the list!");
        }
        /*for (var i = 0; i < this.entities.length; i++)
        {
            if (this.entities[i] == e)
            {
                this.entities.splice(i, 1);
                break;
            }
        }
        log("Trying to delete entity that isn't on the list!");*/
    },
    flashScreen: function(color, time, alpha)
    {
        alpha = alpha || 1.0;
        this.flashAlpha = alpha;
        this.flashColor = color;
        this.flashSpeed = time;
        this.flashTimer = 0;
    },
    fadeScreen: function(color, time)
    {
        this.fadeColor = color;
        this.fadeSpeed = time;
        this.fadeTimer = 0;
    },
    showMessage: function(msg, color, time)
    {
        this.messageTimer = time;
        this.message = msg;
        this.messageColor = color;
    },
    spawnSkeletonSomewhere: function(force)
    {
        force = force || false;
        var found = false;
        var attempts = 0;
        while (!found && attempts < 100)
        {
            var mx = Math.floor(Math.random() * (this.worldSize - 4)) + 2;
            var my = Math.floor(Math.random() * (this.worldSize - 4)) + 2;
            if (distance2D(mx * this.tileSize + this.tileSize / 2.0, 
                my * this.tileSize + this.tileSize / 2.0, 
                this.player.x, this.player.y) < 12.0 * this.tileSize) continue; //No spawn camping
            if (force) this.setMap(mx, my, null);
            if (this.map[mx][my] == null)
            {
                //Check to see if there's something else already there (such as another skeleton)
                for (var i = 0; i < this.entities.length; i++)
                {
                    if (Math.floor(this.entities[i].x / this.tileSize) == mx
                        && Math.floor(this.entities[i].y / this.tileSize) == my)
                    {
                        continue;
                    }
                }
                found = true;
                for (var i = -1; i < 2; i++)
                {
                    for (var j = -1; j < 2; j++)
                    {
                        this.setMap(mx+i, my+j, null);
                    }
                }
                var skele = new Skeleton(mx * this.tileSize + this.tileSize / 2.0, my * this.tileSize + this.tileSize / 2.0,
                    Math.random() * TWOPI);
                this.addEnt(skele);
            }
            attempts++;
        }
    }
};

function TitleState()
{
    this.options = [
        "Resume Game",
        "New Game",
        "Configure Game",
        "Instructions"
    ];
    this.optionIndex = 0;
    this.optionStart = 0;
}
TitleState.prototype = 
{
    update: function(deltaTime)
    {
        if (gKeyHits[13]) //Enter
        {
            switch(this.optionIndex)
            {
                case 0:
                    ChangeState(gGameState);
                    break;
                case 1:
                    gGameState = new GameState(gGameSetupState.options[0].value, 
                        gGameSetupState.options[1].value, gGameSetupState.options[2].value);
                    ChangeState(gGameState);
                    break;
                case 2:
                    ChangeState(gGameSetupState);
                    break;
                case 3:
                    gTutorState = new TutorState(); //Must be updated to reflect latest control scheme
                    ChangeState(gTutorState);
                    break;
            }
        }
        if (gKeyHits[38]) this.optionIndex--; //up
        if (gKeyHits[40]) this.optionIndex++; //down
        if (this.optionIndex < this.optionStart) this.optionIndex = this.options.length - 1;
        if (this.optionIndex >= this.options.length) this.optionIndex = this.optionStart;
    },
    render: function(deltaTime)
    {
        gContext.fillStyle = "#000000";
        gContext.fillRect(0, 0, gWidth, gHeight);
        gContext.fillStyle = "#FFFFFF";
        gContext.font = "24pt impact";
        gContext.textAlign = "center";
        gContext.fillText("DILAN vs. TREES", gHalfWidth, gHalfHeight - 96);
		gContext.font = "12pt impact";
		gContext.fillText("Post Compo Version", gHalfWidth, gHalfHeight - 64);
        gContext.font = "16pt arial";
        gContext.fillText("Up/Down: Move cursor. Enter: Select.", gHalfWidth, gHeight - 24);
        for (var i = this.optionStart; i < this.options.length; i++)
        {
            var prefix = "";
            var suffix = "";
            if (i == this.optionIndex)
            {
                prefix = ">";
                suffix = "<";
            }
            gContext.fillText(prefix + this.options[i] + suffix, gHalfWidth, gHalfHeight + i * 24);
        }
    },
    enter: function()
    {
        if (gGameState)
        {
            this.optionIndex = 0;
            this.optionStart = 0;
        }
        else
        {
            this.optionStart = 1;
        }
    },
    leave: function()
    {

    }
};

function WinState()
{
    this.flashSpeed = 1.0;
    this.flashTimer = this.flashSpeed;
}
WinState.prototype = 
{
    update: function(deltaTime)
    {
        if (gKeyHits[13] || gKeyHits[27]) //enter || escape
        {
            gGameState = null;
            ChangeState(gTitleState);
        }
    },
    render: function(deltaTime)
    {
        gContext.fillStyle = "#000000";
        gContext.fillRect(0, 0, gWidth, gHeight);
        var red = Math.floor(Math.sin(gTimer * 0.25) * 127.0) + 128;
        var green = Math.floor(Math.cos(gTimer * 0.5) * 127.0) + 128;
        var blue = Math.floor(Math.sin(gTimer + Math.PI / 6.9) * 127.0) + 128;
        gContext.fillStyle = "rgb(" + red + "," + green + "," + blue + ")";
        gContext.font = "24pt impact";
        gContext.textAlign = "center";
        gContext.fillText("CONG-RATS, DILAN!", gHalfWidth, gHalfHeight - 64);
        gContext.fillText("YOU HAVE CONQUERED THE TREES!", gHalfWidth, gHalfHeight);
        gContext.fillStyle = "#FFFFFF";
        gContext.font = "16pt arial";
        gContext.fillText("Press ENTER/ESCAPE to return to whence you came.", gHalfWidth, gHalfHeight + 80);

        this.flashTimer -= deltaTime;
        if (this.flashTimer > 0)
        {
            gContext.fillStyle = "rgba(255,255,255," + (this.flashTimer / this.flashSpeed) + ")";
            gContext.fillRect(0, 0, gWidth, gHeight);
        }
    },
    enter: function()
    {
        this.flashTimer = this.flashSpeed;
        SetSong(null);
    },
    leave: function()
    {

    }
};

function LoseState()
{
    this.flashSpeed = 1.0;
    this.flashTimer = this.flashSpeed;
}
LoseState.prototype = 
{
    update: function(deltaTime)
    {
        if (gKeyHits[13] || gKeyHits[27]) //enter || escape
        {
            gGameState = null;
            ChangeState(gTitleState);
        }
    },
    render: function(deltaTime)
    {
        gContext.fillStyle = "#000000";
        gContext.fillRect(0, 0, gWidth, gHeight);
        var red = Math.floor(Math.sin(gTimer) * 64.0) + 191;
        gContext.fillStyle = "rgb(" + red + ",0,0)";
        gContext.font = "24pt impact";
        gContext.textAlign = "center";
        gContext.fillText("YOUR BODY HAS FAILED YOU!", gHalfWidth, gHalfHeight - 64);
        gContext.fillStyle = "#FFFFFF";
        gContext.font = "16pt arial";
        gContext.fillText("Press ENTER/ESCAPE to return to whence you came.", gHalfWidth, gHalfHeight + 80);

        this.flashTimer -= deltaTime;
        if (this.flashTimer > 0)
        {
            gContext.fillStyle = "rgba(255,0,0," + (this.flashTimer / this.flashSpeed) + ")";
            gContext.fillRect(0, 0, gWidth, gHeight);
        }
    },
    enter: function()
    {
        this.flashTimer = this.flashSpeed;
        SetSong(null);
    },
    leave: function()
    {

    }
};

function TutorState()
{
    this.texts = [
    "You are Dilan.",
    "A mysterious demon has placed your friends into a cursed",
    "forest. It grows constantly and rapidly, and everyone will",
    "soon run out of breathing space. You and your trusty",
    "flamethrower have seen much worse, but you still don't know",
    "where your friends are, and the demon's undead army now knows",
    "of your presence.",
    ""];
    switch(gGameSetupState.options[2].value)
    {
        case CTRL_TWINSTICK:
            this.texts.push("-Use the WASD keys to move and strafe",
            "-Use LEFT and RIGHT to turn",
            "-Hold SPACE to use the flamethrower",
            "-Press Q when you have enough crystals",
            "to make things go by a little quicker");
            break;
        case CTRL_WOLFENSTEIN:
            this.texts.push("-Use LEFT and RIGHT to turn",
            "-Use UP and DOWN to move forward and backward",
            "-Hold X and use LEFT/RIGHT to strafe",
            "-Hold Z to use the flamethrower",
            "-Press C when you have enough crystals",
            "to make things go by a little quicker");
            break;
        case CTRL_WOLFENSTEIN2:
            this.texts.push("-Use A and D to turn",
            "-Use W and S to move forward and backward",
            "-Hold J and use A/D to strafe",
            "-Hold K to use the flamethrower",
            "-Press L when you have enough crystals",
            "to make things go by a little quicker");
            break;
    }
    this.texts.push("-Press ESCAPE or ENTER to pause and change settings");
    this.texts.push("-Find your friends. Don't get boned.");
}
TutorState.prototype = 
{
    update: function(deltaTime)
    {
        if (gKeyHits[13] || gKeyHits[27]) //enter || escape
        {
            ChangeState(gTitleState);
        }
    },
    render: function(deltaTime)
    {
        gContext.fillStyle = "#000000";
        gContext.fillRect(0, 0, gWidth, gHeight);
        gContext.textAlign = "center";
        gContext.fillStyle = "#FFFFFF";
        gContext.font = "16pt arial";
        for (var i = 0; i < this.texts.length; i++)
        {
            gContext.fillText(this.texts[i], gHalfWidth, 32 + i * 24);
        }
        gContext.fillText("Press ENTER/ESCAPE to go back", gHalfWidth, gHalfHeight + 192);
    },
    enter: function(deltaTime)
    {

    },
    leave: function(deltaTime)
    {

    }
};

const OPT_MAPSIZE = 0;
const OPT_FOV = 1;
const OPT_CTRL = 2;
const OPT_DIFF = 3;
const OPT_MUVOL = 4;
const OPT_SOVOL = 5;
function GameSetupState()
{
    this.options = [{name: "Map Size: ", value: 64}, 
        {name: "FOV: ", value: 60},
        {name: "Control Scheme: ", value: CTRL_TWINSTICK, string: "Twin Stick"},
        {name: "Game Difficulty: ", value: 0.7, string: "70%"},
        {name: "Music Volume: ", value: 0.5, string: "50%"},
        {name: "Sound Volume: ", value: 1.0, string: "100%"}, 
        {name: "Back", value: ""}];
    this.optionIndex = 0;
}
GameSetupState.prototype = 
{
    update: function(deltaTime)
    {
        if ((gKeyHits[13] && this.optionIndex == this.options.length - 1) 
            || gKeyHits[27]) //enter || escape
        {
            ChangeState(gTitleState);
        }
        if (gKeyHits[39] || gKeyHits[37]) //right || left
        {
            var direction = 1.0;
            if (gKeyHits[37]) direction = -1.0;
            switch(this.optionIndex)
            {
                case OPT_MAPSIZE:
                    this.options[OPT_MAPSIZE].value += 16 * direction;
                    this.options[OPT_MAPSIZE].value = Math.max(32, Math.min(64, this.options[OPT_MAPSIZE].value));
                    break;
                case OPT_FOV:
                    this.options[OPT_FOV].value += 10 * direction;
                    this.options[OPT_FOV].value = Math.max(30.0, Math.min(this.options[OPT_FOV].value, 90.0));
                    break;
                case OPT_CTRL:
                    this.options[OPT_CTRL].value += 1 * direction;
                    if (this.options[OPT_CTRL].value < 0) this.options[OPT_CTRL].value = 2;
                    if (this.options[OPT_CTRL].value > 2) this.options[OPT_CTRL].value = 0;
                    switch (this.options[OPT_CTRL].value)
                    {
                        case CTRL_TWINSTICK:
                            this.options[OPT_CTRL].string = "Twin Stick";
                            break;
                        case CTRL_WOLFENSTEIN:
                            this.options[OPT_CTRL].string = "Wolfenstein";
                            break;
                        case CTRL_WOLFENSTEIN2:
                            this.options[OPT_CTRL].string = "Left-Handed Wolfenstein";
                            break;
                    }
                    break;
                case OPT_DIFF:
                    this.options[OPT_DIFF].value += 0.1 * direction;
                    this.options[OPT_DIFF].value = Math.max(0.1, Math.min(1.0, this.options[OPT_DIFF].value));
                    this.options[OPT_DIFF].string = Math.floor((this.options[OPT_DIFF].value + 0.001) * 100.0) + "%";
                    break;
                case OPT_SOVOL:
                case OPT_MUVOL:
                    this.options[this.optionIndex].value += 0.1 * direction;
                    this.options[this.optionIndex].value = Math.max(0.0, Math.min(1.0, this.options[this.optionIndex].value));
                    this.options[this.optionIndex].string = Math.floor((this.options[this.optionIndex].value + 0.001) * 100.0) + "%";
                    break;
            }
        }
        if (gKeyHits[38]) this.optionIndex--; //up
        if (gKeyHits[40]) this.optionIndex++; //down
        if (this.optionIndex < 0) this.optionIndex = this.options.length - 1;
        if (this.optionIndex >= this.options.length) this.optionIndex = 0;
    },
    render: function(deltaTime)
    {
        gContext.fillStyle = "#000000";
        gContext.fillRect(0, 0, gWidth, gHeight);
        gContext.textAlign = "center";
        gContext.fillStyle = "#FFFFFF";
        gContext.font = "48pt impact";
        gContext.fillText("GAME SETUP", gHalfWidth, gHalfHeight - 128);
        gContext.font = "16pt arial";
        gContext.fillText("Up/Down: Move cursor. Left/Right: Adjust. Enter: Select.", gHalfWidth, gHalfHeight - 64);
        for (var i = 0; i < this.options.length; i++)
        {
            var prefix = "";
            var suffix = "";
            if (i == this.optionIndex)
            {
                prefix = ">";
                suffix = "<";
            }
            gContext.fillText(prefix + this.options[i].name + (this.options[i].string || this.options[i].value) + suffix, gHalfWidth, gHalfHeight + i * 20);
        }
    },
    enter: function()
    {

    },
    leave: function()
    {

    }
};

function Animation(frameX, frameY, frameSize, frames, speed)
{
    this.originX = frameX;
    this.originY = frameY;
    this.frameX = frameX;
    this.frameY = frameY;
    this.frameSize = frameSize;
    this.frames = frames;
    this.speed = speed;
    this.timer = 0;
    this.frame = 0;
}
Animation.prototype = 
{
    update: function(deltaTime)
    {
        this.timer += deltaTime;
        if (this.timer > this.speed)
        {
            this.timer = 0;
            this.frame++;
            if (this.frame >= this.frames.length)
            {
                this.frame = 0;
            }
            this.frameX = this.originX + this.frames[this.frame] * this.frameSize;
        }
    }
};