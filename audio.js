var gAudios = [];
var gGameMusic = new Audio("song_0.ogg");
var gBendMusic = new Audio("song_1.ogg");
var gCurrentSong = null;
var gFadeSong = false;
var gSongFadeSpeed = 0.25;
var gLastSong = null;

function UpdateAudio(deltaTime)
{
    for (var i = 0; i < gAudios.length; i++)
    {
        if (gAudios[i].ended)
        {
            gAudios.splice(i,1);
        }
    }
    if (gDebugMode && gKeyHits[101]) //Numpad 5
    {
        if (gCurrentSong)
        {
            gCurrentSong.currentTime = gCurrentSong.duration - 2.0;
        }
    }
    if (gCurrentSong)
    {
        if (gCurrentSong.loop == false && gCurrentSong.ended && gLastSong)
        {
            SetSong(gLastSong, gLastSong.loop);
        }
        if (gFadeSong == true)
        {
            gCurrentSong.volume = Math.max(0, gCurrentSong.volume - deltaTime * gSongFadeSpeed);
            if (gCurrentSong.volume <= 0)
            {
                gFadeSong = false;
                SetSong(null);
            }
        }
        else
        {
            gCurrentSong.volume = gGameSetupState.options[OPT_MUVOL].value;
        }
    }
}

function PlaySound(path, volume, variate)
{
    if (gGameSetupState.options[OPT_SOVOL].value <= 0.0) return;
    for (var i = 0; i < gAudios.length; i++)
    {
        var otherPath = gAudios[i].src.split('/');
        var ourPath = path.split('/');
        if (otherPath[otherPath.length - 1] == ourPath[ourPath.length - 1])
        {
            //Prevent two identical sound from being played at the same time, resulting in loudness.
            if (gAudios[i].currentTime <= 0.05) 
            {
                return;
            } 
        }
    }
    var sound = new Audio(path);
    sound.play();
    sound.volume = gGameSetupState.options[OPT_SOVOL].value * (volume || 1.0);
    gAudios.push(sound);
}

function SetSong(song, loop)
{
    if (gCurrentSong) gCurrentSong.pause();
    gLastSong = gCurrentSong;
    gCurrentSong = song;
    if (gCurrentSong) 
    {
        gCurrentSong.loop = loop;
        gCurrentSong.play();
        gCurrentSong.currentTime = 0.0;
        gCurrentSong.volume = gGameSetupState.options[OPT_MUVOL].value;;
    }
}